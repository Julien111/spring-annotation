package com.luv2code.springdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OtherRandomFortuneService implements FortuneService {
	
	@Value("${fortune.montant}")
	private String fortuneOne;
	
	@Value("${fortune.montantdeux}")
	private String fortuneTwo;
	
	@Value("${fortune.montanttrois}")
	private String fortuneThree;
	
	private Random random = new Random();

	public OtherRandomFortuneService() {
		super();
	}

	@Override
	public String getFortune() {
		List<String> arrFortune = new ArrayList();
		arrFortune.add(this.fortuneOne);
		arrFortune.add(this.fortuneThree);
		arrFortune.add(this.fortuneTwo);		
		int num = this.random.nextInt(arrFortune.size());
		return arrFortune.get(num);
	}

}
