package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class HockeyCoach implements Coach {
	
	@Autowired
	@Qualifier("happyFortuneService")
	private FortuneService fortuneService;

	public HockeyCoach() {
		super();
	}

	@Override
	public String getDailyWorkOut() {		
		return "Il faut aimer la glace";
	}

	@Override
	public String getDailyFortune() {		
		return fortuneService.getFortune();
	}
	
//	@Autowired
//	public void ownMethodInject(FortuneService fortuneService) {
//		System.out.println("inside setter method");
//		this.fortuneService = fortuneService;
//	}
	

}
