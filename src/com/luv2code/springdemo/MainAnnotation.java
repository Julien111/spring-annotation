package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainAnnotation {

	public static void main(String[] args) {
		//read the code
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//get the bean
		
		TennisCoach tennis = context.getBean("tennisCoach", TennisCoach.class);
		
		HockeyCoach hoc = context.getBean("hockeyCoach", HockeyCoach.class);
		
		//use the code
		
		System.out.println(tennis.getDailyWorkOut());
		System.out.println(tennis.getDailyFortune());
		System.out.println(hoc.getDailyWorkOut());
		System.out.println(hoc.getDailyFortune());
		System.out.println(tennis.getEmail());
		
		//close
		context.close();
	}

}
