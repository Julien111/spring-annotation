package com.luv2code.springdemo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
//@Scope("prototype")
public class TennisCoach implements Coach {
	
	@Value("${sport.email}")
	private String email;
	
	@Value("${sport.team}")
	private String team;
	
	private Random random = new Random();
	
	private FortuneService fortuneService;

	public TennisCoach() {
		super();
	}	
	
	@Autowired
	public TennisCoach(@Qualifier("otherRandomFortuneService") FortuneService fortuneService) {
		super();
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkOut() {
		return "I love Tennis";
	}
		
	@Override
	public String getDailyFortune() {		
		return fortuneService.getFortune();
	}	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	@PostConstruct
	public void initStr () throws Exception {	
		
		 BufferedReader br  = new BufferedReader(new FileReader("src/fortunes.txt"));
		 String str;
		 List<String> arrTest = new ArrayList<>();
		 
		 while ((str = br.readLine()) != null) {
				str = str.substring(8);
				arrTest.add(str);
		}
		 int num = this.random.nextInt(arrTest.size());
		System.out.println("Le composant s'initialise avec le fortune : " + arrTest.get(num));
	}
		
	@PreDestroy
	public void destroy() throws Exception {
		System.out.println("Le composant se détruit");
	}


}
