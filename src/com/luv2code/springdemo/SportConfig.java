package com.luv2code.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//@ComponentScan("com.luv2code.springdemo")
@PropertySource("classpath:sport.properties")
public class SportConfig {
	
	//bean fortune service
	@Bean
	public FortuneService seeFortuneService() {
		return new SeeFortuneService();
	}
	
	//bean fortune service
		@Bean
		public FortuneService happyFortuneService() {
			return new HappyFortuneService();
		}
	
	
	//bean swim coach	
	@Bean
	public Coach swimCoach () {
		SwimCoach mySwimCoach = new SwimCoach(seeFortuneService());		
		return mySwimCoach;
	}
	
	//bean volley coach	
		@Bean
		public Coach volleyCoach () {
			VolleyballCoach myVolleyCoach = new VolleyballCoach(happyFortuneService());		
			return myVolleyCoach;
		}

}
