package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoApp {

	public static void main(String[] args) {
		// read the code
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// get the bean
		TennisCoach tennis = context.getBean("tennisCoach", TennisCoach.class);

		TennisCoach tennis2 = context.getBean("tennisCoach", TennisCoach.class);
		
		boolean result = (tennis == tennis2);

		// use the code
		
		System.out.println("Same object : " + result);
		
		System.out.println("memory location : " + tennis);
		System.out.println("memory location : " + tennis2);

		// close
		context.close();

	}

}
