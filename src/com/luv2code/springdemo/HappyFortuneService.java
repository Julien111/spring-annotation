package com.luv2code.springdemo;

import org.springframework.stereotype.Component;

@Component
public class HappyFortuneService implements FortuneService {

	public HappyFortuneService() {
		super();
	}

	@Override
	public String getFortune() {		
		return "It's your lucky day";
	}

}
