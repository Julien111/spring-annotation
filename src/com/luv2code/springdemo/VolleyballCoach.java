package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;


public class VolleyballCoach implements Coach {
	
	private FortuneService fortuneService;
	
	@Value("${sport.email}")
	private String email;

	public VolleyballCoach() {
		super();
	}	
	
	@Autowired
	public VolleyballCoach(@Qualifier("happyFortuneService") FortuneService theFortuneService) {
		super();
		this.fortuneService = theFortuneService;
	}

	@Override
	public String getDailyWorkOut() {		
		return "Entraînement au service volley";
	}

	@Override
	public String getDailyFortune() {		
		return fortuneService.getFortune();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	

}
