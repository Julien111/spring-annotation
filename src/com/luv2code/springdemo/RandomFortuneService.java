package com.luv2code.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {
	
	private Random random = new Random();
	
	private String[] data = {"Beware wolf", "Diligence is the mother of good luck", "The journey is the reward"};

	public RandomFortuneService() {
		super();
	}

	@Override
	public String getFortune() {		
		//définir un index aléatoire
		int num = this.random.nextInt(data.length);
		return "The fortune is : " + data[num];
	}

}
