package com.luv2code.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainDemoTwoAnnotation {

	public static void main(String[] args) {
		//read the code
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
		
		//get the bean
		
		VolleyballCoach volley = context.getBean("volleyCoach", VolleyballCoach.class);
		
		
		//use the code
		
		System.out.println(volley.getDailyWorkOut());
		System.out.println(volley.getDailyFortune());
		System.out.println(volley.getEmail());
						
		//close
		context.close();
	}
	

}
