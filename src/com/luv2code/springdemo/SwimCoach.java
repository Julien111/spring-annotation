package com.luv2code.springdemo;

import org.springframework.beans.factory.annotation.Value;

public class SwimCoach implements Coach {
	
	@Value("${sport.email}")
	private String email;
	
	@Value("${sport.team}")
	private String team;
	
	private FortuneService fortuneService;

	public SwimCoach() {
		super();		
	}	

	public SwimCoach(FortuneService fortuneService) {
		super();
		this.fortuneService = fortuneService;
	}

	@Override
	public String getDailyWorkOut() {		
		return "Swim 1000 meters for a warm up ! ";
	}

	@Override
	public String getDailyFortune() {		
		return fortuneService.getFortune();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}	

}
