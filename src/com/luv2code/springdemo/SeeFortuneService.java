package com.luv2code.springdemo;

public class SeeFortuneService implements FortuneService {

	public SeeFortuneService() {
		super();
	}

	@Override
	public String getFortune() {		
		return "you see now !";
	}

}
